<?php
/**
 * @package     Bubu.Template
 * @subpackage  Layout
 *
 * @copyright   Copyright (C) 2019 bubutechnologies.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Access\Access;
use Joomla\Registry\Registry;

\JLoader::register('TagsHelperRoute', JPATH_BASE . '/components/com_tags/helpers/route.php');

?>
<?php if (!empty($displayData)) : ?>
	<div class="tags">
		<span class="tags-title">Tags:</span>
		<?php foreach ($displayData as $i => $tag) : ?>
			<?php if (in_array($tag->access, Access::getAuthorisedViewLevels(Factory::getUser()->get('id')))) : ?>
				<?php if ($i != 0): ?>, <?php endif; ?>
				<?php $tagParams = new Registry($tag->params); ?>
				<?php $link_class = $tagParams->get('tag_link_class', 'tag-link'); ?>
					<a href="<?php echo Route::_(\TagsHelperRoute::getTagRoute($tag->tag_id . ':' . $tag->alias)) ?>" class="<?php echo $link_class; ?>">
						<?php echo $this->escape($tag->title); ?>
					</a>
			<?php endif; ?>
		<?php endforeach; ?>
	</div>
<?php endif; ?>
