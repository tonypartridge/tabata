<?php
/**
 * @package     Joomla.Site
 * @subpackage  Layout
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

extract($displayData);

$group = isset($group) ? $group : null;

$field = $form->getField($name, $group);
$field->hint = $field->description;
$field->description = '';
$field->class .= 'appearance-none block w-full bg-grey-lighter text-grey-darker border rounded border-grey-light py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white';

echo $field->renderField();
