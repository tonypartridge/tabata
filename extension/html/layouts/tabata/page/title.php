<?php
/**
 * @package     Bubu.Template
 * @subpackage  Tabata
 *
 * @copyright   Copyright (C) 2019 bubutechnologies.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Layout variables
 *
 * @var  string   $title  Title to be shown
 */
extract($displayData);
?>
<h1 class="page-title mb-4 text-3xl"><?=$title?></h1>
