<?php
/**
 * @package     Bubu.Template
 * @subpackage  Tabata
 *
 * @copyright   Copyright (C) 2019 bubutechnologies.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Uri\Uri;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Language\Text;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Language\Associations;

\JLoader::register('ContentHelperRoute', JPATH_SITE . '/components/com_content/helpers/route.php');
HTMLHelper::addIncludePath(JPATH_SITE . '/components/com_content/helpers/html');

/**
 * Layout variables
 *
 * @var  stdClass   $article  Article as it comes from joomla models/helpers
 */
extract($displayData);

$params = $article->params;

$canEdit = $params->get('access-edit');
$info    = $params->get('info_block_position', 0);

$assocParam = (\JLanguageAssociations::isEnabled() && $params->get('show_associations'));

$link = Route::_(
	ContentHelperRoute::getArticleRoute(
		$article->slug, $article->catid, $article->language
	)
);

$categoryLink = Route::_(
		ContentHelperRoute::getCategoryRoute($article->catslug
	)
);

$showTags = $info == 0 && $params->get('show_tags', 1) && !empty($article->tags->itemTags);
?>
<div class="content-article w-full mb-6">
	<div class="flex flex-col justify-between leading-normal">
		<?php echo LayoutHelper::render('tabata.com_content.article.image-full', ['article' => $article, 'options' => ['class' => 'w-full']]); ?>
		<div class="mb-2">
			<?=LayoutHelper::render('tabata.page.title', ['title' => $this->escape($article->title)])?>
			<?php echo $article->event->afterDisplayTitle; ?>
			<div class="flex items-center my-4 px-4 py-2 text-sm bg-grey-lighter">
					<p class="text-grey-dark mr-2">
						<?php echo Text::sprintf('COM_CONTENT_WRITTEN_BY',''); ?>
					</p>
					<p class="text-grey-darker leading-none mr-4">
						<span itemprop="name"><?php echo ($article->created_by_alias ?: $article->author); ?></span>
					</p>
					<?php if ($params->get('show_category')): ?>
						<p class="text-grey-dark border-l border-grey-light pl-4 mr-2">
							<?php echo Text::sprintf('COM_CONTENT_CATEGORY', ''); ?>
						</p>
						<p class="text-green-darker hover:text-green-dark leading-none mr-4">
							<a href="<?php echo $categoryLink; ?>"><?php echo $this->escape($article->category_title) ?></a>
						</p>
					<?php endif; ?>
					<p class="text-grey-dark border-l border-grey-light pl-4">
						<?php echo HTMLHelper::_('date', $article->created, Text::_('DATE_FORMAT_LC3')); ?>
					</p>
			</div>
			<div class="text-grey-darker text-base">
				<?php if (!empty($article->event->beforeDisplayContent)): ?>
					<div class="content-article__before-content mb-4">
						<?php echo $article->event->beforeDisplayContent; ?>
					</div>
				<?php endif; ?>
				<?php echo $article->text; ?>
				<?php if (!empty($article->event->afterDisplayContent)): ?>
					<div class="content-article__after-content mt-4">
						<?php echo $article->event->afterDisplayContent; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
		<?php if ($showTags) : ?>
			<div class="py-4">
				<div class="">
					<?php echo LayoutHelper::render('tabata.com_content.article.tags', ['article' => $article]); ?>
				</div>
			</div>
		<?php endif; ?>
	</div>
</div>