<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\LayoutHelper;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidator');

?>
<div class="w-full reset-confirm<?php echo $this->pageclass_sfx; ?>">
	<?=LayoutHelper::render('tabata.page.title', ['title' => Text::_('TPL_TABATA_LBL_ENTER_VERIFICATION_CODE')])?>
	<form action="<?php echo JRoute::_('index.php?option=com_users&task=reset.confirm'); ?>" method="post" class="form-validate form-horizontal well">
		<?php foreach ($this->form->getFieldsets() as $fieldset) : ?>
			<?php $fields = $this->form->getFieldset($fieldset->name); ?>
			<fieldset>
				<?php if (isset($fieldset->label)) : ?>
					<p class="text-grey-darker mb-4"><?php echo JText::_($fieldset->label); ?></p>
				<?php endif; ?>
				<?php foreach ($fields as $field) : ?>
					<?php
					if ('Spacer' === $field->type)
					{
						continue;
					}
					?>
					<?php
					$field->description = '';
					$field->labelclass = 'block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2';
					$field->class = 'appearance-none block w-full bg-grey-lighter text-grey-darker border rounded border-grey-light py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white';
					?>
					<?php echo $field->renderField(); ?>
				<?php endforeach; ?>
			</fieldset>
		<?php endforeach; ?>
		<div class="mt-4">
			<button class="button-primary validate" type="submit">
				<?php echo JText::_('JSUBMIT'); ?>
			</button>
		</div>
		<?php echo JHtml::_('form.token'); ?>
	</form>
</div>
