<?php

use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\LayoutHelper;
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidator');

?>
<div class="w-full registration<?php echo $this->pageclass_sfx; ?>">
	<form id="member-registration" action="<?php echo JRoute::_('index.php?option=com_users&task=registration.register'); ?>" method="post" class="form-validate form-horizontal well" enctype="multipart/form-data">
		<?php // Iterate through the form fieldsets and display each one. ?>
		<?php foreach ($this->form->getFieldsets() as $fieldset) : ?>
			<?php $fields = $this->form->getFieldset($fieldset->name); ?>
			<?php if (count($fields)) : ?>
				<fieldset class="mb-4">
					<?php // If the fieldset has a label set, display it as the legend. ?>
					<?php if (isset($fieldset->label)) : ?>
						<?=LayoutHelper::render('tabata.page.title', ['title' => Text::_($fieldset->label)])?>
					<?php endif; ?>
					<div class="lg:flex">
						<div class="lg:flex-auto">
							<?=LayoutHelper::render('tabata.form.field', ['name' => 'name', 'form' => $this->form])?>
						</div>
						<div class="lg:flex-auto lg:ml-6">
							<?=LayoutHelper::render('tabata.form.field', ['name' => 'username', 'form' => $this->form])?>
						</div>
					</div>
					<div class="lg:flex">
						<div class="lg:flex-auto">
							<?=LayoutHelper::render('tabata.form.field', ['name' => 'password1', 'form' => $this->form])?></div>
						<div class="lg:flex-auto lg:ml-6"><?=LayoutHelper::render('tabata.form.field', ['name' => 'password2', 'form' => $this->form])?></div>
					</div>
					<div class="lg:flex">
						<div class="lg:flex-auto"><?=LayoutHelper::render('tabata.form.field', ['name' => 'email1', 'form' => $this->form])?></div>
						<div class="lg:flex-auto lg:ml-6"><?=LayoutHelper::render('tabata.form.field', ['name' => 'email2', 'form' => $this->form])?></div>
					</div>
					<?=$this->form->renderField('captcha')?>
				</fieldset>
			<?php endif; ?>
		<?php endforeach; ?>
		<div>
			<button class="button-primary mr-6" type="submit">
				<?php echo JText::_('JREGISTER'); ?>
			</button>
			<a class="text-grey-darker hover:text-grey" href="<?php echo JRoute::_(''); ?>" title="<?php echo JText::_('JCANCEL'); ?>">
				<?php echo JText::_('JCANCEL'); ?>
			</a>
			<input type="hidden" name="option" value="com_users" />
			<input type="hidden" name="task" value="registration.register" />
		</div>
		<?php echo JHtml::_('form.token'); ?>
	</form>
</div>
