<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_search
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Language\Text;

$options = [
	[
		'text' => Text::_('COM_SEARCH_ALL_WORDS'),
		'value' => 'all'
	],
	[
		'text' => Text::_('COM_SEARCH_ANY_WORDS'),
		'value' => 'any'
	],
	[
		'text' => Text::_('COM_SEARCH_EXACT_PHRASE'),
		'value' => 'exact'
	]
];
?>
<div class="flex mt-6 lg:mt-0">
	<?php foreach ($options  as $i => $option) : ?>
		<div class="flex items-center mr-4">
			<label for="radio<?=$i?>" class="flex items-center cursor-pointer">
				<input id="radio<?=$i?>" type="radio" name="searchphrase" class="mr-1" value="<?=$option['value']?>" <?php if ($this->searchphrase === $option['value']):?>checked<?php endif;?> />
					<?=$option['text']?>
			</label>
		</div>
	<?php endforeach; ?>
</div>
