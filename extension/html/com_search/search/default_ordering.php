<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_search
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Language\Text;

$options = [
	[
		'text' => Text::_('COM_SEARCH_NEWEST_FIRST'),
		'value' => 'newest'
	],
	[
		'text' => Text::_('COM_SEARCH_OLDEST_FIRST'),
		'value' => 'oldest'
	],
	[
		'text' => Text::_('COM_SEARCH_MOST_POPULAR'),
		'value' => 'popular'
	],
	[
		'text' => Text::_('COM_SEARCH_ALPHABETICAL'),
		'value' => 'alpha'
	],
	[
		'text' => Text::_('JCATEGORY'),
		'value' => 'category'
	],
];
?>
<div class="relative">
	<select id="ordering" name="ordering"
		class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
		onchange="this.form.submit()"
	>
		<?php foreach ($options as $option): ?>
			<?php $selected = $this->ordering === $option['value'] ?>
			<option value="<?=$option['value']?>" <?php if ($selected): ?>selected="selected"<?php endif; ?>><?=$option['text']?></option>
		<?php endforeach; ?>
	</select>
	<div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-black">
	  <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
	</div>
</div>
