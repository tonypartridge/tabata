<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_search
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Language\Text;

$options = [];

for ($i = 5; $i <= 30; $i += 5)
{
	$options[] = [
		'text'  => $i,
		'value' => $i
	];
}

$options[] = ['text'  => Text::_('J50'), 'value' => 50];
$options[] = ['text'  => Text::_('J100'), 'value' => 100];
$options[] = ['text'  => Text::_('JALL'), 'value' => 0];

$limit = 0;

foreach ($options as $option)
{
	if ($this->pagination->limit === $option['value'])
	{
		$limit = $option['value'];
	}
}
?>
<div class="relative">
	<select id="limit" name="limit"
		class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
		onchange="this.form.submit()"
	>
		<?php foreach ($options as $option): ?>
			<?php $selected = $limit === $option['value'] ?>
			<option value="<?=$option['value']?>" <?php if ($selected): ?>selected="selected"<?php endif; ?>><?=$option['text']?></option>
		<?php endforeach; ?>
	</select>
	<div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-black">
      <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
    </div>
</div>
