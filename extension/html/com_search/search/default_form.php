<?php

use Joomla\CMS\Language\Text;
/**
 * @package     Joomla.Site
 * @subpackage  com_search
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('bootstrap.tooltip');

$lang = JFactory::getLanguage();
$upperLimit = $lang->getUpperLimitSearchWord();

?>
<form id="searchForm" action="<?php echo JRoute::_('index.php?option=com_search'); ?>" method="post">
	<div class="lg:flex items-center mb-6">
		<div class="flex-1 mr-2 search__box">
			<label for="search-searchword" class="hidden">
				<?php echo JText::_('COM_SEARCH_SEARCH_KEYWORD'); ?>
			</label>
			<input type="text" name="searchword"
				title="<?php echo JText::_('COM_SEARCH_SEARCH_KEYWORD'); ?>"
				placeholder="<?php echo JText::_('COM_SEARCH_SEARCH_KEYWORD'); ?>"
				id="search-searchword" size="30"
				maxlength="<?php echo $upperLimit; ?>"
				value="<?php echo $this->escape($this->origkeyword); ?>"
				class="w-full inline-block bg-grey-lighter align-middle appearance-none border rounded border-grey-light w-full py-2 px-3 text-grey-darker focus:outline-none focus:bg-white focus:shadow-outline"
			 />
		</div>
		<?php if ($this->params->get('search_phrases', 1)) : ?>
			<?php echo $this->loadTemplate('phrases'); ?>
		<?php endif; ?>
	</div>
	<input type="hidden" name="task" value="search" />
	<?php if ($this->params->get('search_areas', 1)) : ?>
		<fieldset class="only mb-4">
			<div class="lg:flex flex-wrap items-center">
				<h3 class="text-xl mr-6 whitespace-no-wrap">
					<?php echo JText::_('COM_SEARCH_SEARCH_ONLY'); ?>
				</h3>
				<div class="flex mt-4 lg:mt-0">
					<?php foreach ($this->searchareas['search'] as $val => $txt) : ?>
						<?php $checked = is_array($this->searchareas['active']) && in_array($val, $this->searchareas['active']) ? 'checked="checked"' : ''; ?>
						<label for="area-<?php echo $val; ?>" class="flex flex-no-wrap checkbox mr-5 cursor-pointer">
							<input type="checkbox" name="areas[]"
							value="<?php echo $val; ?>"
							class="mr-2"
							id="area-<?php echo $val; ?>" <?php echo $checked; ?>
							/>
							<?php echo JText::_($txt); ?>
						</label>
					<?php endforeach; ?>
				</div>
			</div>
		</fieldset>
	<?php endif; ?>

	<button name="Search"
		onclick="this.form.submit()"
		class="button-primary mr-2"
		title="<?php echo JHtml::_('tooltipText', 'COM_SEARCH_SEARCH');?>"
	>
		<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>
	</button>
	<div class="flex justify-end items-center">
		<div class="ordering-box flex items-center">
			<label for="ordering" class="ordering mr-2">
				<?php echo JText::_('COM_SEARCH_ORDERING'); ?>
			</label>
			<?php echo $this->loadTemplate('ordering'); ?>
		</div>
		<?php if ($this->total > 0) : ?>
			<div class="flex items-center ml-4">
				<label for="limit" class="mr-2">
					<?php echo Text::_('JGLOBAL_DISPLAY_NUM'); ?>
				</label>
				<?php echo $this->loadTemplate('limit'); ?>
			</div>
		<?php endif; ?>
	</div>
</form>
