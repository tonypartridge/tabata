<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_search
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\LayoutHelper;

$title = empty($this->origkeyword) ? Text::_('TPL_TABATA_LBL_SEARCH') : Text::sprintf('TPL_TABATA_LBL_SEARCH_RESULTS_FOR', $this->escape($this->origkeyword));

?>
<div class="search<?php echo $this->pageclass_sfx; ?>">
	<?=LayoutHelper::render('tabata.page.title', ['title' => $title])?>
	<?php echo $this->loadTemplate('form'); ?>
	<?php if ($this->error == null && count($this->results) > 0) : ?>
		<?php echo $this->loadTemplate('results'); ?>
	<?php else : ?>
		<?php echo $this->loadTemplate('error'); ?>
	<?php endif; ?>
</div>
