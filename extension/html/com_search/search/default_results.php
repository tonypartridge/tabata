<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_search
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Language\Text;
use Joomla\CMS\HTML\HTMLHelper;

?>
<div class="search-results mt-6 <?php echo $this->pageclass_sfx; ?>">
<?php foreach ($this->results as $result) : ?>
	<div class="search-results__result mb-6">
		<div class="flex">

			<div class="result-title text-xl flex-1">
				<?php echo $this->pagination->limitstart + $result->count . '. '; ?>
				<?php if ($result->href) : ?>
					<a href="<?php echo JRoute::_($result->href); ?>"<?php if ($result->browsernav == 1) : ?> target="_blank"<?php endif; ?>
						class="link-primary "
					>
						<?php // $result->title should not be escaped in this case, as it may ?>
						<?php // contain span HTML tags wrapping the searched terms, if present ?>
						<?php // in the title. ?>
						<?php echo $result->title; ?>
					</a>
				<?php else : ?>
					<?php // see above comment: do not escape $result->title ?>
					<?php echo $result->title; ?>
				<?php endif; ?>
			</div>
			<?php if ($this->params->get('show_date')) : ?>
				<div class="result-created text-gray-600<?php echo $this->pageclass_sfx; ?>">
					<?php echo HTMLHelper::_('date', $result->created, Text::_('DATE_FORMAT_LC3')); ?>
				</div>
			<?php endif; ?>
		</div>
		<div class="result-text text-gray-700">
			<?php echo $result->text; ?>
		</div>
	</div>
<?php endforeach; ?>
</div>
<div class="pagination">
	<?php echo $this->pagination->getPagesLinks(); ?>
</div>
<div class="flex">
	<div class="flex-1 searchintro<?php echo $this->params->get('pageclass_sfx'); ?>">
		<?php if (!empty($this->searchword)) : ?>
			<?php echo Text::plural('TPL_TABATA_LBL_N_RESULTS_FOUND', '<span class="badge badge-info">' . $this->total . '</span>'); ?>
		<?php endif; ?>
	</div>
	<?php if ($this->total > 0) : ?>
		<div>
			<?php echo $this->pagination->getPagesCounter(); ?>
		</div>
	<?php endif; ?>
</div>
