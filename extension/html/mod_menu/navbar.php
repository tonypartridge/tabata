<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Layout\LayoutHelper;

$id = '';

if (($tagId = $params->get('tag_id', '')))
{
	$id = ' id="' . $tagId . '"';
}

$user = Factory::getUser();

$moduleClass = htmlspecialchars($params->get('moduleclass_sfx', ''), ENT_COMPAT, 'UTF-8');
$profileLink = Route::_('index.php?option=com_users&view=profile');
?>
<nav class="flex items-center justify-between flex-wrap <?php echo $moduleClass; ?> ">
	<a href="<?=JUri::root()?>" class="flex items-center flex-no-shrink text-green-lighter hover:text-white mr-10">
		<span class="mr-2"><?php echo LayoutHelper::render('tabata.logo.svg'); ?></span>
		<span class="font-semibold text-2xl tracking-tight">Tabata</span>
	</a>
	<div class="block lg:hidden">
		<button class="navbar-burger flex items-center px-3 py-2 border rounded text-green-light hover:text-white">
		<svg class="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="https://www.w3.org/2000/svg"><title>Menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/></svg>
		</button>
	</div>
	<div id="main-nav" class="w-full block flex-grow lg:flex lg:items-center lg:w-auto hidden <?php echo $class_sfx; ?>" <?php echo $id; ?>>
		<div class="text-sm lg:flex-grow">
			<?php foreach ($list as $i => &$item) : ?>
				<?php
					$item->anchor_css = 'block mt-4 text-lg p-1 lg:inline-block lg:mt-0 hover:text-white mr-4 item-' . $item->id . ' ' . $item->anchor_css;

					if ($item->id == $default_id)
					{
						$item->anchor_css .= ' default';
					}

					if (($item->id == $active_id) || ($item->type == 'alias' && $item->params->get('aliasoptions') == $active_id))
					{
						$item->anchor_css .= ' current text-white';
					}
					else
					{
						$item->anchor_css .= ' text-green-lighter';
					}

					if (in_array($item->id, $path))
					{
						$item->anchor_css .= ' active';
					}

					if ($item->deeper)
					{
						$item->anchor_css .= ' deeper';
					}

					if ($item->parent)
					{
						$item->anchor_css .= ' parent';
					}

					switch ($item->type) :
						case 'separator':
						case 'component':
						case 'heading':
						case 'url':
							require JModuleHelper::getLayoutPath('mod_menu', 'navbar_' . $item->type);
							break;

						default:
							require JModuleHelper::getLayoutPath('mod_menu', 'navbar_url');
							break;
					endswitch;
				?>
			<?php endforeach; ?>
		</div>
		<?php if (!$user->guest) : ?>
				<a href="<?php echo $profileLink; ?>" class="flex items-center flex-no-shrink text-teal-lighter hover:text-white">
					<svg class="fill-current h-7 w-7 mr-2" width="35" height="35"  version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1000 1000" enable-background="new 0 0 1000 1000" xml:space="preserve">
						<metadata>User icon</metadata>
						<g><path d="M500,990C229.4,990,10,770.6,10,500S229.4,10,500,10s490,219.4,490,490S770.6,990,500,990z M769,848.9c-45.2-104.1-148.2-177.4-269-177.4s-223.9,73.1-269.1,177.3C305.3,906.4,398.6,941,500,941C601.4,941,694.5,906.5,769,848.9L769,848.9z M647,451c0-81.2-65.8-147-147-147c-81.2,0-147,65.8-147,147c0,81.2,65.8,147,147,147C581.2,598,647,532.2,647,451z M500,59C256.5,59,59,256.5,59,500c0,123.8,51.1,235.5,133.3,315.6c44.8-91.8,129-160.8,231-184.2C353.2,601.6,304,532,304,451c0-108.2,87.8-196,196-196c108.2,0,196,87.8,196,196c0,81-49.2,150.6-119.3,180.4c102,23.3,186.1,92.4,231,184.2C889.8,735.6,941,623.8,941,500C941,256.5,743.5,59,500,59L500,59z"/></g>
					</svg>
					<?php echo $user->get('name'); ?>
				</a>
		<?php endif; ?>
  </div>
</nav>

