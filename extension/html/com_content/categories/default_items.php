<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Layout\LayoutHelper;

JHtml::_('bootstrap.tooltip');

$class = ' class="first"';
$lang  = JFactory::getLanguage();

if ($this->maxLevelcat != 0 && count($this->items[$this->parent->id]) > 0) :
?>
	<?php foreach ($this->items[$this->parent->id] as $id => $item) : ?>
		<?php echo LayoutHelper::render('tabata.com_content.category.preview', ['category' => $item, 'params' => $this->params]); ?>
		<?php if (count($item->getChildren()) > 0 && $this->maxLevelcat > 1) : ?>
			<?php
			$this->items[$item->id] = $item->getChildren();
			$this->parent = $item;
			$this->maxLevelcat--;
			?>
			<?php foreach ($this->items[$item->id] as $item) : ?>
				<?php echo LayoutHelper::render('tabata.com_content.category.preview', ['category' => $item, 'params' => $this->params]); ?>
			<?php endforeach; ?>
			<?php
				$this->parent = $item->getParent();
				$this->maxLevelcat++;
			?>
		<?php endif; ?>
	<?php endforeach; ?>
<?php endif; ?>
