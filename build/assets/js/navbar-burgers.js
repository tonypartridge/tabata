(function() {
  'use strict';

  var navToggle = function (event) {
    // Get the "main-nav" element
    var $target = document.getElementById('main-nav');

    // Toggle the class on "main-nav"
    $target.classList.toggle('hidden');
  }

  function init() {
    // Let GC remove unneeded parts
    document.removeEventListener('DOMContentLoaded', init);

    // Get all "navbar-burger" elements
    var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

    // Check if there are any navbar burgers
    if ($navbarBurgers.length) {
      // Add a click event on each of them
      $navbarBurgers.forEach(function($el) {
        $el.addEventListener('click', navToggle);
      });
    }
  }

  document.addEventListener("DOMContentLoaded", init);
})();
