(function() {
  'use strict';

  var trackScroll = function () {
    var goTopBtn = document.getElementById('back-to-top');

    if (goTopBtn) {
      var scrolled = window.pageYOffset;
      var coords = document.documentElement.clientHeight;

      if (scrolled > coords) {
        goTopBtn.classList.remove('hidden');
      }
      if (scrolled < coords) {
        goTopBtn.classList.add('hidden');
      }
    }
  }

  var backToTop = function () {
    if (window.pageYOffset > 0) {
      window.scrollBy(0, -80);
      setTimeout(backToTop, 0);
    }
  }

  var init = function () {
    // Let GC remove unneeded parts
    document.removeEventListener('DOMContentLoaded', init);

    var goTopBtn = document.getElementById('back-to-top');

    if (goTopBtn) {
      window.addEventListener('scroll', trackScroll);
      goTopBtn.addEventListener('click', backToTop);
    }
  }

  document.addEventListener("DOMContentLoaded", init);
})();
