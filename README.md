# Tabata - Tailwind CSS template for Joomla!

Fast template prototyping with [Tailwind CSS](https://tailwindcss.com/docs) for [Joomla!](https://joomla.org)  

![Load an article](./docs/img/tabata-preview.png)

## Quick start.

This template comes with a build system based in [Gulp.js](https://gulpjs.com/). It assumes you have a local joomla website to test the template with. Once you have the local site make a note of its location. Example site could be stored in:  

`/var/www/joomla-cms`

The site will usually be the site you are customising the template for.

You will need to have NPM installed in your workstation. Follow instructions on [npmjs.com](https://www.npmjs.com/get-npm).  

Once you have the site locally follow these steps:

1. Enter in this repo `build` folder:
    * Run `cd build`
1. Duplicate `gulp-config.dist.json` to `gulp-config.json`
2. Edit `build/gulp-config.json` to point dev site folder to your local joomla site. Example:  
    * "devSite": "/htdocs/joomla-cms"
3. Install NPM dependencies:
    * Run `npm install`
4. You can start working with the template running:
    * `gulp`

The previous command will launch a watch task and the system will be waiting your changes to css files. It also opens the browser pointing to your local website so you can see the changes live.   

## Demo mode

You can see a demo of available utilities by adding `?tmpl=demo` when navigating through your site. 

## Gulp Commands

* `gulp clean`: Clean all the template files from the local joomla site.
* `gulp copy`: Copy all the template files to the local joomla site.
* `gulp css`: Compile template CSS files.
* `gulp dist`: Prepare CSS & Javascript files to be published online/distributed. This takes extra time because it purges unused CSS styles and other stuff not recommended while developing the template.
* `gulp distCSS`: Prepare CSS files to be published online/distributed.
* `gulp distJS`: Prepare Javascript files to be published online/distributed.
* `gulp js`: Compile Javascript files.
* `gulp release`: Create a ZIP with the template ready to be installed on any Joomla website. Files are created in `build/releases` folder.  
* `gulp tailwind`: Compile TailwindCSS files. This will be required if you change the `tailwind.config.js` file to add colors, sizes, etc.
* `gulp watch`: Start watching for changes in the template repository.

## Tools used 

* [Gulp.js](https://gulpjs.com/) - Build system based on Javascript.
* [PostCss](https://postcss.org/) - CSS post-processor for advanced features.
* [Purgecss](https://www.purgecss.com/) - Remove CSS code that is not actually used.
* [clean-css](https://github.com/jakubpawlowicz/clean-css) - Fast and efficient CSS optimizer
* [Browsersync](https://www.browsersync.io/) - Time-saving synchronised browser testing.

## Copyright & License <a id="license"></a>

Tailwind CSS is licensed under [MIT LICENSE](https://github.com/tailwindcss/tailwindcss/blob/master/LICENSE).

This is why this template is also licensed under [MIT LICENSE](./LICENSE).  

Copyright (C) 2019 [Bubu Technologies](http://bubutechnologies.com) - All rights reserved.  
